Pod::Spec.new do |s|

  s.name         = "GD-FIPS"
  s.version      = "5.0.0.78.3"
  s.summary      = "The BlackBerry Dynamics Platform provides a range of SDKs and plugins that allow you to develop in the environment of your choice and build native, hybrid, or web apps."
  s.description  = "GD Framework "
  s.homepage     = "https://developers.blackberry.com/us/en/resources/get-started/blackberry-dynamics-getting-started.html?platform=ios#step-2"
  s.license      = { :type => 'BSD' }
  s.author       = "Blackberry"
  s.platform     = :ios, "9.0"
  s.source	 = { :git => "git@gitlab.com:rogerhu/gd-framework-fips.git" }
  s.resources = ["FIPS_module/*"]
  s.compiler_flags = "-lz -lstdc++ -framework WebKit -framework CoreTelephony"
  s.xcconfig =  { 'ENABLE_BITCODE' => 'NO',
                  'LDPLUSPLUS' => '${SRCROOT}/FIPS_module/$(CURRENT_ARCH).sdk/bin/gd_fipsld',
                  'LD' => '${SRCROOT}/FIPS_module/$(CURRENT_ARCH).sdk/bin/gd_fipsld'}
end